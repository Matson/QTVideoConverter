#include "videowidgetsurface.h"
#include <QWidget>
#include <QtMultimedia>

VideoWidgetSurface::VideoWidgetSurface(QWidget *widget, QObject *parent)
    : QAbstractVideoSurface(parent)
    , widget(widget)
    , imageFormat(QImage::Format_Invalid)
{
}

QList<QVideoFrame::PixelFormat> VideoWidgetSurface::supportedPixelFormats(
        QAbstractVideoBuffer::HandleType handleType) const
{
    if (handleType == QAbstractVideoBuffer::NoHandle) {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_RGB32
                << QVideoFrame::Format_ARGB32
                << QVideoFrame::Format_ARGB32_Premultiplied
                << QVideoFrame::Format_RGB565
                << QVideoFrame::Format_RGB555;
    } else {
        return QList<QVideoFrame::PixelFormat>();
    }
}
/* ---supportedPixelFormats---
 * From the supportedPixelFormats() function we return a list of pixel formats
 * the surface can paint. The order of the list hints at which formats are preferred
 * by the surface. Assuming a 32-bit RGB backbuffer, we'd expect that a 32-bit RGB
 * type with no alpha to be fastest to paint so QVideoFrame::Image_RGB32 is first
 * in the list.
 * Since we don't support rendering using any special frame handles we don't return
 * any pixel formats if handleType is not QAbstractVideoBuffer::NoHandle.
 */


bool VideoWidgetSurface::isFormatSupported(
        const QVideoSurfaceFormat &format, QVideoSurfaceFormat *similar) const
{
    Q_UNUSED(similar);

    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    return imageFormat != QImage::Format_Invalid
            && !size.isEmpty()
            && format.handleType() == QAbstractVideoBuffer::NoHandle;
}

/*---isFormatSupported---
 * In isFormatSupported() we test if the frame type of a surface format maps to
 * a valid QImage format, that the frame size is not empty, and the handle type
 * is QAbstractVideoBuffer::NoHandle. Note that the QAbstractVideoSurface
 * implementation of isFormatSupported() will verify that the list of supported
 * pixel formats returned by supportedPixelFormats(format.handleType()) contains
 * the pixel format and that the size is not empty so a reimplementation wasn't
 * strictly necessary in this case.
 */




bool VideoWidgetSurface::start(const QVideoSurfaceFormat &format)
{
    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    if (imageFormat != QImage::Format_Invalid && !size.isEmpty()) {
        this->imageFormat = imageFormat;
        imageSize = size;
        sourceRect = format.viewport();

        QAbstractVideoSurface::start(format);

        widget->updateGeometry();
        updateVideoRect();

        return true;
    } else {
        return false;
    }
}

/* ---start---
 * To start our surface we'll extract the image format and size from the selected
 * video format and save it for use in the paint() function. If the image format,
 * or size are invalid then we'll set an error and return false. Otherwise we'll
 * save the format and confirm the surface has been started, by calling
 * QAbstractVideoSurface::start(). Finally since the video size may have changed
 * we'll trigger an update of the widget, and video geometry.
 * */





void VideoWidgetSurface::stop()
{
    currentFrame = QVideoFrame();
    targetRect = QRect();

    QAbstractVideoSurface::stop();

    widget->update();
}
/* ---stop---
 * When the surface is stopped we need to release the current frame and invalidate
 * the video region. Then we confirm the surface has been stopped by calling
 * QAbstractVideoSurface::stop() which sets the started state to false and finally
 * we update so the video widget so paints over the last frame.*/




bool VideoWidgetSurface::present(const QVideoFrame &frame)
{
    if (surfaceFormat().pixelFormat() != frame.pixelFormat()
            || surfaceFormat().frameSize() != frame.size()) {
        setError(IncorrectFormatError);
        stop();

        return false;
    } else {
        currentFrame = frame;

        widget->repaint(targetRect);

        return true;
    }
}

/* ---present---
 * We can't paint from outside a paint event, so when a new frame is received in
 * present() we save a reference to it and force an immediate repaint of the video
 * region. We retain the saved reference to the frame after the repaint so that the
 * widget can be repainted between frame changes if necessary.

 * If the format of the frame doesn't match the surface format we can't paint it
 * or very likely any future frames. So we set an UnsupportedFormatError on our
 * surface and stop it immediately.
 */


void VideoWidgetSurface::updateVideoRect()
{
    QSize size = surfaceFormat().sizeHint();
    size.scale(widget->size().boundedTo(size), Qt::KeepAspectRatio);

    targetRect = QRect(QPoint(0, 0), size);
    targetRect.moveCenter(widget->rect().center());
}

/* ---updateVideoRect---
 * The updateVideoRect() function calculates the region within the widget the video
 * occupies. The size hint of the video format gives a suggested size for the video
 * calculated from the viewport and pixel aspect ratio. If the suggested size fits
 * within the widget then we create a new rect of that size in the center of the
 * widget. Otherwise we shrink the size maintaining the aspect ratio so that it
 * does fit.
 */



void VideoWidgetSurface::paint(QPainter *painter)
{
    if (currentFrame.map(QAbstractVideoBuffer::ReadOnly)) {
        const QTransform oldTransform = painter->transform();

        if (surfaceFormat().scanLineDirection() == QVideoSurfaceFormat::BottomToTop) {
           painter->scale(1, -1);
           painter->translate(0, -widget->height());
        }

        QImage image(
                currentFrame.bits(),
                currentFrame.width(),
                currentFrame.height(),
                currentFrame.bytesPerLine(),
                imageFormat);

        painter->drawImage(targetRect, image, sourceRect);

        painter->setTransform(oldTransform);

        currentFrame.unmap();
    }
}

/* ---paint---
 * The paint() function is called by the video widget to paint the current video
 * frame. Before we draw the frame first we'll check the format for the scan line
 * direction and if the scan lines are arranged from bottom to top we'll flip the
 * painter so the frame isn't drawn upside down. Then using the image format
 * information saved in the start() function we'll construct a new QImage from the
 * current video frame, and draw it to the the widget.
*/


