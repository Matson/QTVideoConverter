#include "videowidget.h"

#include "videowidgetsurface.h"

#include <QtMultimedia>

VideoWidget::VideoWidget(QWidget *parent)
    : QWidget(parent)
    , surface(0)
{
    setAutoFillBackground(false);
    setAttribute(Qt::WA_NoSystemBackground, true);
    setAttribute(Qt::WA_PaintOnScreen, true);

    QPalette palette = this->palette();
    palette.setColor(QPalette::Background, Qt::black);
    setPalette(palette);

    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    surface = new VideoWidgetSurface(this);
}
/* ---Constructior---
 * In the VideoWidget constructor we set some flags to speed up re-paints a little.
 * Setting the Qt::WA_NoSystemBackground flag and disabling automatic background
 * fills will stop Qt from a painting a background that'll be completely obscured
 * by the video. The Qt::WA_PaintOnScreen flag will allow us to paint to the screen
 * instead of the back buffer where supported.

 *Next we set the background color to black, so that any borders around the video are filled in black rather the default background color.

 *Finally we construct an instance of the VideoWidgetSurface class.
*/




VideoWidget::~VideoWidget()
{
    delete surface;
}
/* ---Destructor---
 * In the destructor we simply delete the VideoWidgetSurface instance.
 */


QSize VideoWidget::sizeHint() const
{
    return surface->surfaceFormat().sizeHint();
}

/* ---sizeHint---
 * We get the size hint for the widget from the video format of the surface which
 * is calculated from viewport and pixel aspect ratio of the video format.
*/



void VideoWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    if (surface->isActive()) {
        const QRect videoRect = surface->videoRect();

        if (!videoRect.contains(event->rect())) {
            QRegion region = event->region();
            //region.subtract(videoRect);
            //nie ma funckji substract, dalem substracted nie wiem czy
            //to dobrze
            region.subtracted(videoRect);
            QBrush brush = palette().background();

            foreach (const QRect &rect, region.rects())
                painter.fillRect(rect, brush);
        }

        surface->paint(&painter);
    } else {
        painter.fillRect(event->rect(), palette().background());
    }
}

/* ---paintEvent---
 * When the video widget receives a paint event we first check if the surface is
 * started, if not then we simply fill the widget with the background color. If
 * it is then we draw a border around the video region clipped by the paint region,
 * before calling paint on the video surface to draw the current frame.
*/



void VideoWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    surface->updateVideoRect();
}
/* ---resizeEvent---
 * The resizeEvent() function is reimplemented to trigger an update of the video
 * region when the widget is resized.*/

