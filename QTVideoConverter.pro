#-------------------------------------------------
#
# Project created by QtCreator 2016-05-21T12:38:31
#
#-------------------------------------------------

QT       += core gui
QT       += multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTVideoConverter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    videowidgetsurface.cpp \
    videowidget.cpp

HEADERS  += mainwindow.h \
    videowidgetsurface.h \
    videowidget.h

FORMS    += mainwindow.ui

RESOURCES += \
    Icons/icons.qrc
