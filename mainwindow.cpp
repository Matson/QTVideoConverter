#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "videowidget.h"
#include <QtMultimedia>
#include <QFileDialog>
#include <QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    surface(0)
{
    ui->setupUi(this);
    //initPlayer();

    connect(&movie, SIGNAL(stateChanged(QMovie::MovieState)),
            this, SLOT(movieStateChanged(QMovie::MovieState)));
    connect(&movie, SIGNAL(frameChanged(int)),
            this, SLOT(frameChanged(int)));

    VideoWidget *videoWidget = new VideoWidget;
    surface = videoWidget->videoSurface();




    //QAbstractButton *openButton = new QPushButton(tr("Open..."));
    connect(ui->openButton, SIGNAL(clicked()), this, SLOT(openFile()));

    ui->playButton->setEnabled(false);
    connect(ui->playButton, SIGNAL(clicked()), this, SLOT(play()));
    /*
    playButton = new QPushButton;
    playButton->setEnabled(false);
    playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));

    connect(playButton, SIGNAL(clicked()),
            this, SLOT(play()));*/

    ui->videoSlider->setRange(0,0);
    connect(ui->videoSlider, SIGNAL(sliderMoved(int)), this, SLOT(setPosition(int)));
    connect(&movie, SIGNAL(frameChanged(int)), this, SLOT(setPosition(int)));
    /*
    positionSlider = new QSlider(Qt::Horizontal);
    positionSlider->setRange(0, 0);

    connect(positionSlider, SIGNAL(sliderMoved(int)),
            this, SLOT(setPosition(int)));

    connect(&movie, SIGNAL(frameChanged(int)),
            positionSlider, SLOT(setValue(int)));*/

    //ui->widget = videoWidget; //bad idea?

    /*
    QBoxLayout *controlLayout = new QHBoxLayout;
    controlLayout->setMargin(0);
    controlLayout->addWidget(openButton);
    controlLayout->addWidget(playButton);
    controlLayout->addWidget(positionSlider);

    QBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(videoWidget);
    layout->addLayout(controlLayout);

    setLayout(layout);*/

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile()
{
    QStringList supportedFormats;
    foreach (QString fmt, QMovie::supportedFormats())
        supportedFormats << fmt;
    foreach (QString fmt, QImageReader::supportedImageFormats())
        supportedFormats << fmt;

    QString filter = "Images (";
    foreach ( QString fmt, supportedFormats) {
        filter.append(QString("*.%1 ").arg(fmt));
    }
    filter.append(")");

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Movie"),
            QDir::homePath(), filter);

    if (!fileName.isEmpty()) {
        surface->stop();

        movie.setFileName(fileName);

        ui->playButton->setEnabled(true);
        ui->videoSlider->setMaximum(movie.frameCount());

        movie.jumpToFrame(0);
    }
}

void MainWindow::play()
{
    switch(movie.state()) {
    case QMovie::NotRunning:
        movie.start();
        break;
    case QMovie::Paused:
        movie.setPaused(false);
        break;
    case QMovie::Running:
        movie.setPaused(true);
        break;
    }
}


void MainWindow::movieStateChanged(QMovie::MovieState state)
{
    //do poprawienia ikonki zeby sie na
    //nasze zmieniała
    switch(state) {
    case QMovie::NotRunning:
    case QMovie::Paused:
        ui->playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
        break;
    case QMovie::Running:
        ui->playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
        break;
    }
}

void MainWindow::frameChanged(int frame)
{
    if (!presentImage(movie.currentImage())) {
        movie.stop();
        ui->playButton->setEnabled(false);
        ui->videoSlider->setMaximum(0);
    } else {
        ui->videoSlider->setValue(frame);
    }
}

void MainWindow::setPosition(int frame)
{
    movie.jumpToFrame(frame);
}

bool MainWindow::presentImage(const QImage &image)
{
    QVideoFrame frame(image);

    if (!frame.isValid())
        return false;

    QVideoSurfaceFormat currentFormat = surface->surfaceFormat();

    if (frame.pixelFormat() != currentFormat.pixelFormat()
            || frame.size() != currentFormat.frameSize()) {
        QVideoSurfaceFormat format(frame.size(), frame.pixelFormat());

        if (!surface->start(format))
            return false;
    }

    if (!surface->present(frame)) {
        surface->stop();

        return false;
    } else {
        return true;
    }
}



/*void MainWindow::initPlayer(){
    player = new QMediaPlayer;
    player->setMedia(QUrl::fromLocalFile("C://Users//Mateusz//Desktop//priv//antos_gamer.mp4"));

    videoWidget = new QVideoWidget;
    player->setVideoOutput(videoWidget);
    this->setCentralWidget(videoWidget);

    //auto L = static_cast<QVBoxLayout *>(layout());
    //L->insertWidget(1,videoWidget);

    videoWidget->show();
    player->play();
}*/

