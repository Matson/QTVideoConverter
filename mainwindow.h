#ifndef MAINWINDOW_H
#define MAINWINDOW_H
//#include <QtMultimedia>
//#include <QtMultimediaWidgets>
#include <QtGui/QMovie>
#include <QtGui>


class QAbstractButton;
class QAbstractVideoSurface;
class QSlider;

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void openFile();
    void play();

private slots:
    void movieStateChanged(QMovie::MovieState state);
    void frameChanged(int frame);
    void setPosition(int frame);

private:
    Ui::MainWindow *ui;
    //QMediaPlayer *player;
    //QVideoWidget *videoWidget;
    //void initPlayer();
    bool presentImage(const QImage &image);

    QMovie movie;
    QAbstractVideoSurface *surface;
    QAbstractButton *playButton;
    QSlider *positionSlider;
};

#endif // MAINWINDOW_H

